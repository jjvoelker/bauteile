# README

Bauteile - a CRUDsy web app for organizing electronic parts

Things you may want to cover:

* Ruby/Rails version: 2.7.4/7.0.0

* Model created with: bin/rails generate model Part manufacturer:string mpn:string description:string datasheet:string

* Database initialization
