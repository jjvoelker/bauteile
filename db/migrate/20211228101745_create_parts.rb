class CreateParts < ActiveRecord::Migration[7.0]
  def change
    create_table :parts do |t|
      t.string :manufacturer
      t.string :mpn
      t.string :description
      t.string :datasheet

      t.timestamps
    end
  end
end
